package sbu.cs;

import java.util.Random;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        Random rnd = new Random();
        String s = "";
        for (int i = 0; i < length; i++) {
            char tmp = (char)(rnd.nextInt(26) + 'a');
            s += tmp;
        }
        return s;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
            if (length < 3)
                throw new IllegalValueException();
            Random rnd = new Random();
            StringBuilder tmp = new StringBuilder(weakPassword(length));
            char[] d = {'0', '1','2','3','4','5','6','7','8','9'};
            int g = rnd.nextInt(length) + 1;
        for (int i = 0; i < g; i++) {
            int r1 = rnd.nextInt((length - 1)/2);
            int rG = rnd.nextInt(10);
            tmp.setCharAt(r1,d[rG]);
        }
        char[] s = {'?','@','!','<','>','#','%','^','(',')','%','*','_','-','.','/','='};
        int q = rnd.nextInt(length) + 1;
        for (int i = 0;i < q;i++) {
            int r1 = rnd.nextInt((length - 1)/2) + (length - 1)/2;
            int rand = rnd.nextInt(17);
            tmp.setCharAt(r1,s[rand]);
        }
        return tmp.toString();
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public int fibonacci(int n) {
        int t1 = 1, t2 = 1, e = 0;
        if (n == 1 || n == 2)
            return 1;
        if (n > 2)
        {
            for(int i = 1;i <=n ; i++)
            {
                e = t1 + t2;
                t1 = t2;
                t2 = e;
            }
            return e;
        }
        return 1;
    }
    public boolean isFiboBin(int n) {
        for (int i = 0; i < n; i++) {
            int fibo = fibonacci(i);
            String b = Integer.toBinaryString(fibo);
            int t = 0;
            for (int j = 0; j < b.length(); j++) {
                if (b.charAt(j) == '1')
                    t++;
            }
            int a = fibo + t;
            if (a == n)
                return true;
        }
        return false;
    }
}
