package sbu.cs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long sum = 0;
        for ( int i = 0 ; i < arr.length; i++) {
            if (i % 2 == 0)
                sum += arr[i];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int[] reversearry = new  int[arr.length];
        int j = arr.length;
        for (int i = 0; i < arr.length; i++) {
            reversearry[i] = arr[j];
            j--;
        }
        return  reversearry;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][]  matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        double[][] ar = new double[m1.length][m2[0].length];
        if (m1.length == m2[0].length) {
            for (int i = 0; i < m1.length; i++) {
                for (int j = 0; j < m1.length; j++) {
                    for (int t = 0; t < m2[0].length; t++) {
                        for (int p = 0; p < m2[0].length; p++) {
                            ar[i][t] += (m1[i][j] * m2[p][t]);
                        }
                    }
                }
            }
        }
        if(m1.length != m2[0].length)
            throw new RuntimeException("not same dim");

        return ar;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names ) {
        List<List<String>> r = new ArrayList<>();
        int s = names.length;
        for (int i = 0; i < s; i++)
        {
            List<String> tmp = new ArrayList<>();
            int columns = names[i].length;
            for (int j = 0; j < columns; j++)
            {
                tmp.add(names[i][j]);
            }
            r.add(tmp);
        }
        return r;
        }



    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public boolean Prime(int n)
    {
        if(n <= 1)
        {
            return false;
        }
        for (int i = 2; i < n; i++)
        {
            if(n % i == 0)
            {
                return false;
            }
        }
        return true;
    }

    public List<Integer> primeFactors(int n) {
        List<Integer> r = new ArrayList<>();
        for (int i = 2; i <= n; i++)
        {
            if(n % i == 0 && Prime(i))
            {
                r.add(i);
            }
        }
        Collections.sort(r);
        return r;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        List<String> r = new ArrayList<>();
        String[] w = line.split(" ");
        for (String tmp : w)
        {
            String toAdd = tmp.replaceAll("[^a-zA-Z]","");
            r.add(toAdd);
        }
        return r;
    }
}
        
