package sbu.cs;
import java.util.*;

public class ExerciseLecture4 {
   
    public long factorial(int n)
    {


        long factryl = 1;
        for(int i = 1;i <= n;i++)
        {
            factryl = i * factryl;
        }
        long L = factryl;

        return L;
    }

    public long fibonacci(int n) {


        int t1 = 1, t2 = 1, e = 0;

        if (n == 1 || n == 2)
            return 1;

        if (n > 2)
        {
            for(int i = 1;i <=n ; i++)
            {
                e = t1 + t2;
                t1 = t2;
                t2 = e;
            }
            return e;
        }
        return 1;
    }

    public String reverse(String word) {
        StringBuilder name = new StringBuilder(word);
        return name.reverse().toString();
    }

    public boolean isPalindrome(String line) {
        String b = line.replaceAll(" ", "");
        String s = reverse(b);
        boolean n = b.equalsIgnoreCase(s);
        return n;
    }

    public char[][] dotPlot(String str1, String str2) {
        char[][] array = new char[str1.length()][str2.length()];

        for (int i = 0; i < str1.length(); i++) {
            for (int j = 0; j < str2.length(); j++) {
                if (str1.charAt(i) == str2.charAt(j))
                    array[i][j] = '*';
                else {
                    array[i][j] = ' ';
                }
            }

        }
        return array;
    }
}
