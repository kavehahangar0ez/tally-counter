package sbu.cs;

public class TallyCounter implements TallyCounterInterface
{
    private int number;
    public TallyCounter()
    {
        number = 0;
    }
    @Override
    public void count()
    {
        if(number < 9999)
        {
            number++;
        }
    }
    @Override
    public int getValue()
    {
        return number;
    }
    @Override
    public void setValue(int value) throws IllegalValueException
    {
        if(value < 0 || value > 9999)
        {
            throw new IllegalValueException();
        }
        number = value;
    }
}